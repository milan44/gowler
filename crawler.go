package gowler

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"time"
)

type Gowler struct {
	HttpClient    http.Client
	UrlMatchRegex regexp.Regexp
	Async         bool
	Parallelism   int
	OnNewLink     func(string) bool
	OnRequest     func(*http.Request) bool
	OnResponse    func(Response)
	OnError       func(Response, error)
	OpenRequests  int
	Queue         []string
	Logger        func(string)
}

type Response struct {
	Response *http.Response
	URL      string
	Body     string
	Header   http.Header
	BaseURL  string
}

func NewGowler(client http.Client, urlMatchRegex regexp.Regexp) Gowler {
	return Gowler{
		HttpClient:    client,
		UrlMatchRegex: urlMatchRegex,
		Async:         false,
		Parallelism:   4,
		OnNewLink: func(str string) bool {
			fmt.Println("Added " + str)
			return true
		},
		OnRequest: func(r *http.Request) bool {
			return true
		},
		OnResponse: func(r Response) {
			fmt.Println("Crawled " + r.URL)
		},
		OnError: func(r Response, err error) {
			fmt.Println("Failed to crawl " + r.URL + " (" + err.Error() + ")")
		},
		Logger: func(str string) {
			fmt.Println(str)
		},
	}
}

func (g *Gowler) Start(startUrls []string) {
	for _, uri := range startUrls {
		g.Queue = append(g.Queue, uri)
	}
	g.Visit()
}

func (g *Gowler) Visit() {
	for len(g.Queue) > 0 || g.OpenRequests != 0 {
		for g.OpenRequests >= g.Parallelism {
			time.Sleep(1 * time.Second)
		}
		if len(g.Queue) == 0 {
			time.Sleep(1 * time.Second)
			continue
		}
		uri := g.Queue[0]
		g.Queue = g.Queue[1:]

		g.OpenRequests++

		go func() {
			g.Logger("Visiting '" + uri + "'")

			g.Get(uri)

			g.OpenRequests--
		}()
	}
}

func (g *Gowler) Get(url string) {
	req, _ := http.NewRequest("GET", url, nil)

	if !g.OnRequest(req) {
		return
	}

	response, err := g.HttpClient.Do(req)
	if err != nil {
		resp := Response{
			Response: response,
		}
		if response != nil {
			resp.URL = response.Request.URL.String()
			resp.Body = _bodyToString(response.Body)
			resp.Header = response.Header
			resp.BaseURL = _getBaseUrl(response.Request.URL.String())
		}
		g.OnError(resp, err)
	} else {
		resp := Response{
			Response: response,
			URL:      response.Request.URL.String(),
			Body:     _bodyToString(response.Body),
			Header:   response.Header,
			BaseURL:  _getBaseUrl(response.Request.URL.String()),
		}

		g.OnResponse(resp)

		if resp.BaseURL != "" {
			links := _extractLinksFromHTML(resp.Body, resp.BaseURL)
			for _, link := range links {
				if !g.UrlMatchRegex.MatchString(link) {
					g.Logger("Skipping '" + link + "' (didn't match regex)")
					continue
				}

				if g.OnNewLink(link) {
					g.Queue = append(g.Queue, link)
				}
			}
		}
	}
}

func _bodyToString(body io.ReadCloser) string {
	str, _ := ioutil.ReadAll(body)
	return string(str)
}
func _getBaseUrl(uri string) string {
	u, err := url.Parse(uri)
	if err != nil {
		return ""
	}
	return u.Scheme + "://" + u.Host
}
