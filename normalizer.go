package gowler

import (
	"github.com/goware/urlx"
	"regexp"
	"strings"
)

func _extractLinksFromHTML(html string, baseUrl string) []string {
	re := regexp.MustCompile(`(?mU)<a.+href="(.*)".*>`)
	matches := re.FindAllStringSubmatch(html, -1)

	var links []string

	for _, match := range matches {
		link := _normalizeLink(match[1], baseUrl)
		if link != "" {
			links = append(links, link)
		}
	}

	return links
}

func _normalizeLink(link string, baseUrl string) string {
	link = _extractLink(link, baseUrl)
	if link == "" {
		return ""
	}

	ux, err := urlx.Parse(link)
	if err != nil {
		return ""
	}
	ux.Fragment = ""
	normalized, err := urlx.Normalize(ux)
	if err != nil {
		normalized = ux.String()
	}
	normalized = strings.TrimRight(normalized, "/")
	return normalized
}

func _extractLink(rawLink string, baseUrl string) string {
	testRawLink := strings.ToLower(rawLink)
	if strings.HasPrefix(testRawLink, "http://") || strings.HasPrefix(testRawLink, "https://") {
		return rawLink
	}

	if strings.HasPrefix(rawLink, "//") {
		return "http:" + rawLink
	}

	if strings.HasPrefix(rawLink, "#") {
		return ""
	}

	if !strings.HasSuffix(baseUrl, "/") {
		baseUrl += "/"
	}

	if strings.HasPrefix(rawLink, "/") {
		rawLink = strings.TrimLeft(rawLink, "/")
	}

	return baseUrl + rawLink
}
