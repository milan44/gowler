package main

import (
	"gitlab.com/milan44/gowler"
	"net/http"
	"regexp"
)

func main() {
	reg := regexp.MustCompile(`(?mU)^https*:\/\/[^\/]*\.de`)
	c := gowler.NewGowler(http.Client{}, *reg)
	c.Queue = append(c.Queue, "https://google.de")
	c.Visit()
}
